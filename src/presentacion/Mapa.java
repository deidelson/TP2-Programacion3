package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import datos.Conexion;
import negocio.GrafoPesado;
import negocio.Solver;

//Cargar archivo de coordenadas, escribir instancia1 hasta instancia5
public class Mapa 
{
	private JFrame frmClusteringMap;
	private JMapViewer mapa;
	private GrafoPesado grafo;
	private JMenuItem grafoCompleto;
	private JMenuItem agm;
	private JMenuItem quitarNmasGrandes;
	private JMenuItem quitarMayor;
	private JMenuItem quitarAristasConPesoM;
	private JMenuItem agregarArista;
	private JMenuItem quitarArista;
	private JMenuItem agmN;
	private Conexion conexion;
	private JMenuItem pesoTotalGrafo;
	private JMenuItem pesoPromedioGrafo;
	private JMenuItem cambiarCoordendas;
	private JMenuItem guardarCoordenadas;
	private JMenuItem promedioPorN;
	


	public static void main(String [] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Mapa window = new Mapa();
					window.frmClusteringMap.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mapa()
	{
		initialize();
		conexion=new Conexion();
		grafo=new GrafoPesado();
	}
	
	

	/**
	 * @wbp.parser.entryPoint
	 */

	private void initialize() 
	{
		frmClusteringMap = new JFrame();
		frmClusteringMap.setTitle("Clustering map");
		frmClusteringMap.setBounds(100, 100, 915, 714);
		frmClusteringMap.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		mapa=new JMapViewer();
		mapa.setDisplayPositionByLatLon(-34.521, -58.7008, 11);
		
		
		JMenuBar menuBar = new JMenuBar();
		frmClusteringMap.setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem GuardarSolucion = new JMenuItem("Guardar soluci\u00F3n");
		GuardarSolucion.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(grafo==null)
				{
					JOptionPane.showMessageDialog(null, "Algo salio mal");
					return;
				}
				try
				{
					String nombreDelArchivo=JOptionPane.showInputDialog("Ingrese nombre que desea dar al "
							+ "archivo");
					
					conexion.generarJsonGrafoPesado(grafo, nombreDelArchivo);
					
					JOptionPane.showMessageDialog(null, "Se guardo correctamente con el nombre: "+nombreDelArchivo);
				}
				catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		});
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mntmNuevo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String cant=JOptionPane.showInputDialog("Ingrese cantidad de vertices");
				try
				{
					int cantidad=Integer.parseInt(cant);
					
					grafo=new GrafoPesado(cantidad);
					
					
					for(int i=0; i<cantidad; i++)
					{
						String lat=JOptionPane.showInputDialog("Ingrese latitud del vertice "+i);
						String lon=JOptionPane.showInputDialog("Ingrese longitud del vertice "+i);
						
						Double latn=Double.parseDouble(lat);
						Double lonn=Double.parseDouble(lon);
						
						grafo.setCoordenadas(i, latn, lonn);
						grafo.agregarTodasLasAristas();
					}
					borrarAristas();
					borrarVertices();
					dibujarVertices(grafo);
					List<MapPolygon> poligonosGrafoClus = generarPoligono(grafo);
					dibujarLineas(poligonosGrafoClus);
					clusteringEnTrue();
					edicionEnTrue();
				}
				catch (Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
				dibujarVertices(grafo);
			}
		});
		mnArchivo.add(mntmNuevo);
		
		guardarCoordenadas = new JMenuItem("Guardar coordenadas");
		guardarCoordenadas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String nombreDelArchivo=JOptionPane.showInputDialog("Ingrese el nombre");
				try
				{
					conexion.generarJsonCoordenadas(grafo.getListaCoordenadas(), nombreDelArchivo);
					JOptionPane.showMessageDialog(null, "Se guardo correctamente con el nombre "+nombreDelArchivo);
				}
				catch (Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		
		JMenuItem cargarArchivoC = new JMenuItem("Cargar archivo de coordenadas");
		cargarArchivoC.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String nombreArchivoCoordenadas=JOptionPane.showInputDialog("Ingrese nombre del archivo de coordenadas");
				borrarAristas();
				borrarVertices();
				try
				{
					grafo=new GrafoPesado(nombreArchivoCoordenadas);
					dibujarVertices(grafo);
					edicionEnTrue();
					clusteringEnTrue();
					estadisticasEnTrue();
					JOptionPane.showMessageDialog(null, "Se cargo correctamente");
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null, "Error "+e.getMessage());
				}
			}
		});
		mnArchivo.add(cargarArchivoC);
		
		JMenuItem cargarSolucion = new JMenuItem("Cargar soluci\u00F3n");
		cargarSolucion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				String nombreDelArchivo=JOptionPane.showInputDialog("Ingrese nombre del archivo");
				borrarAristas();
				borrarVertices();
				try
				{
					grafo=conexion.leerGrafo(nombreDelArchivo);
					dibujarVertices(grafo);
					List<MapPolygon> poligonosGrafoClus = generarPoligono(grafo);
					dibujarLineas(poligonosGrafoClus);
					edicionEnFalse();
					clusteringEnTrue();
					estadisticasEnTrue();
					JOptionPane.showMessageDialog(null, "Se cargo correctamente");
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null, "Error "+e.getMessage());
				}
			}
		});
		mnArchivo.add(cargarSolucion);
		mnArchivo.add(guardarCoordenadas);
		mnArchivo.add(GuardarSolucion);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		cambiarCoordendas = new JMenuItem("Cambiar coordendas");
		cambiarCoordendas.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					String vert=JOptionPane.showInputDialog("Ingrese el vertice");
					String la=JOptionPane.showInputDialog("Ingrese la latitud");
					String lo=JOptionPane.showInputDialog("Ingrese la longitud");
					
					int vertice=Integer.parseInt(vert);
					double lat=Double.parseDouble(la);
					double lon=Double.parseDouble(lo);
					
					grafo.setCoordenadas(vertice, lat, lon);
					
					dibujarVertices(grafo);
					List<MapPolygon> poligonosGrafoComun = generarPoligono(grafo);
					dibujarLineas(poligonosGrafoComun);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		mnEdicin.add(cambiarCoordendas);
		
		grafoCompleto = new JMenuItem("Grafo completo");
		mnEdicin.add(grafoCompleto);
		grafoCompleto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				borrarAristas();
				List<MapPolygon> poligonosGrafoComun = generarPoligono(grafo);
				dibujarLineas(poligonosGrafoComun);
			}
		});
		
		
		agm = new JMenuItem("Arbol generador minimo");
		mnEdicin.add(agm);
		agm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				grafo = Solver.AGM(grafo);
				borrarAristas();
				List<MapPolygon> poligonosAGM = generarPoligono(grafo);
				dibujarLineas(poligonosAGM);
				edicionEnFalse();
			}
		});

		agmN = new JMenuItem("Arbol generador minimo empezando por ");
		agmN.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				String v=JOptionPane.showInputDialog("Ingrese el vertice por el cual desea empezar");
				
				int vertice=Integer.parseInt(v);
				if(vertice>=grafo.getCantidadVertices() || vertice<0)
				{
					JOptionPane.showMessageDialog(null,"El vertice no existe");
					return;
				}
				grafo = Solver.AGMempezandoPorVertice(grafo, vertice);
				borrarAristas();
				List<MapPolygon> poligonosAGM = generarPoligono(grafo);
				dibujarLineas(poligonosAGM);
				edicionEnFalse();
				
			}
		});
		mnEdicin.add(agmN);
		
		JMenu clustering = new JMenu("Clustering ");
		mnEdicin.add(clustering);
		
		agregarArista = new JMenuItem("Agregar arista");
		agregarArista.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String vertice1=JOptionPane.showInputDialog("Ingrese el primer vertice");
				String vertice2=JOptionPane.showInputDialog("Ingrese el segundo vertice");
				
				try
				{
					int v1=Integer.parseInt(vertice1);
					int v2=Integer.parseInt(vertice2);
					grafo.agregarArista(v1, v2);
					borrarAristas();
					List<MapPolygon> poligonosClustering = generarPoligono(grafo);
					dibujarLineas(poligonosClustering);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		clustering.add(agregarArista);
		
		quitarArista = new JMenuItem("Quitar Arista");
		quitarArista.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String vertice1=JOptionPane.showInputDialog("Ingrese el primer vertice");
				String vertice2=JOptionPane.showInputDialog("Ingrese el segundo vertice");
				
				try
				{
						Integer v1=Integer.parseInt(vertice1);
						Integer v2=Integer.parseInt(vertice2);
						grafo.quitarArista(v1, v2);
						borrarAristas();
						List<MapPolygon> poligonosClustering = generarPoligono(grafo);
						dibujarLineas(poligonosClustering);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		clustering.add(quitarArista);
		
		promedioPorN = new JMenuItem("Quitar aristas mayores a promedio*n");
		promedioPorN.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				
				String n=JOptionPane.showInputDialog("Ingrese el numero por el que desea multiplicar el promedio");
				
				
				try
				{
						int num=Integer.parseInt(n);
						grafo=Solver.clusteringPromedioPorN(grafo, num);
						borrarAristas();
						List<MapPolygon> poligonosClustering = generarPoligono(grafo);
						dibujarLineas(poligonosClustering);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		
		quitarMayor = new JMenuItem("Quitar la arista mas grande");
		quitarMayor.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					int cantidad=1;
					if(cantidad>grafo.getCantidadAristas())
					{
						JOptionPane.showMessageDialog(null, "La cantidad ingresada debe ser menor a la cantidad de aristas");
					}
					grafo=Solver.clusteringQuitandoPesadas(grafo, cantidad);
					borrarAristas();
					List<MapPolygon> poligonosClustering = generarPoligono(grafo);
					dibujarLineas(poligonosClustering);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		clustering.add(quitarMayor);
		
		quitarNmasGrandes = new JMenuItem("Quitando las \"n\" aristas mas grandes");
		quitarNmasGrandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				String cant=JOptionPane.showInputDialog("Ingrese cantidad de aritas que desea quitar");
				try
				{
					int cantidad= Integer.parseInt(cant);
					if(cantidad>grafo.getCantidadAristas())
					{
						JOptionPane.showMessageDialog(null, "La cantidad ingresada debe ser menor a la cantidad de aristas");
					}
					grafo=Solver.clusteringQuitandoPesadas(grafo, cantidad);
					borrarAristas();
					List<MapPolygon> poligonosClustering = generarPoligono(grafo);
					dibujarLineas(poligonosClustering);
					
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
				
			}
		});
		clustering.add(quitarNmasGrandes);
		clustering.add(promedioPorN);
		
		quitarAristasConPesoM = new JMenuItem("Quitar aristas con peso mayora a");
		quitarAristasConPesoM.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					String cant=JOptionPane.showInputDialog(null, "Ingrese el peso");
					double peso=Double.parseDouble(cant);
					
					grafo=Solver.clusteringQuitandoMayoresA(grafo, peso);
					borrarAristas();
					List<MapPolygon> poligonosClustering = generarPoligono(grafo);
					dibujarLineas(poligonosClustering);
				}
				catch(Exception s)
				{
					JOptionPane.showMessageDialog(null, "Error "+s.getMessage());
				}
			}
		});
		clustering.add(quitarAristasConPesoM);
		
		JMenu mnEstadsticas = new JMenu("Estad\u00EDsticas");
		menuBar.add(mnEstadsticas);
		
		pesoTotalGrafo = new JMenuItem("Peso total del grafo");
		pesoTotalGrafo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				JOptionPane.showMessageDialog(null, "El peso total es de: "+grafo.getPesoTotal());
			}
		});
		mnEstadsticas.add(pesoTotalGrafo);
		
		pesoPromedioGrafo = new JMenuItem("Peso promedio del grafo");
		pesoPromedioGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				JOptionPane.showMessageDialog(null, "Peso promedio del grafo: "+grafo.pesoPromedio());
			}
		});
		mnEstadsticas.add(pesoPromedioGrafo);
		
		edicionEnFalse();
		clusteringEnFalse();
		estadisticasEnFalse();
		frmClusteringMap.setContentPane(mapa);
	}

	//auxiliares
	private void dibujarVertices(GrafoPesado grafo)
	{
		for (int i=0; i< grafo.getCantidadVertices();i++)
		{
			Coordinate coordenadas = new Coordinate (grafo.getCoordenadas(i).getLatitud(),grafo.getCoordenadas(i).getLongitud());
			MapMarkerDot marker=new MapMarkerDot(Integer.toString(i),coordenadas);
			marker.getStyle().setBackColor(Color.ORANGE);
			mapa.addMapMarker(marker);
		}	
	}
	
	private List<MapPolygon> generarPoligono(GrafoPesado grafo)
	{
		List<MapPolygon> poligonos = new ArrayList<MapPolygon>();
		int iterador=0;
		
		for (int i = 0; i < grafo.getCantidadVertices(); i++)
		{
			
			for(Integer j:grafo.getVecinosDe(i))
			{
				
				MapPolygon polygon;
				ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
									
				coordenadas.add(new Coordinate(grafo.getCoordenadas(i).getLatitud(),grafo.getCoordenadas(i).getLongitud()));
				coordenadas.add(new Coordinate(grafo.getCoordenadas(i).getLatitud(),grafo.getCoordenadas(i).getLongitud()));
				coordenadas.add(new Coordinate(grafo.getCoordenadas(j).getLatitud(), grafo.getCoordenadas(j).getLongitud()));
				coordenadas.add(new Coordinate(grafo.getCoordenadas(j).getLatitud(), grafo.getCoordenadas(j).getLongitud()));
				
				polygon = new MapPolygonImpl(coordenadas);
				polygon.getStyle().setColor(Color.BLACK);
				
				poligonos.add(iterador, polygon);
				iterador++;
			}
		}
		
		return poligonos;
	}
	
	private void dibujarLineas(List<MapPolygon> poligonos)
	{
		for (MapPolygon poligon : poligonos)
		{
			mapa.addMapPolygon(poligon);
		}
	}
	
	private void borrarAristas()
	{
		mapa.removeAllMapPolygons();;
	}
	
	private void borrarVertices()
	{
		mapa.removeAllMapMarkers();
	}
	
	private void edicionEnFalse()
	{
		agm.setEnabled(false);
		agmN.setEnabled(false);
		grafoCompleto.setEnabled(false);
		cambiarCoordendas.setEnabled(false);
	}
	
	private void edicionEnTrue()
	{
		agm.setEnabled(true);
		agmN.setEnabled(true);
		grafoCompleto.setEnabled(true);
		cambiarCoordendas.setEnabled(true);
	}
	
	private void clusteringEnFalse()
	{
		quitarNmasGrandes.setEnabled(false);
		quitarMayor.setEnabled(false);
		quitarAristasConPesoM.setEnabled(false);
		agregarArista.setEnabled(false);
		quitarArista.setEnabled(false);
		promedioPorN.setEnabled(false);
	}
	
	private void clusteringEnTrue()
	{
		quitarNmasGrandes.setEnabled(true);
		quitarMayor.setEnabled(true);
		quitarAristasConPesoM.setEnabled(true);
		agregarArista.setEnabled(true);
		quitarArista.setEnabled(true);
		promedioPorN.setEnabled(true);
	}
	
	private void estadisticasEnFalse()
	{
		pesoTotalGrafo.setEnabled(false);
		pesoPromedioGrafo.setEnabled(false);
	}
	
	private void estadisticasEnTrue()
	{
		
		pesoTotalGrafo.setEnabled(true);
		pesoPromedioGrafo.setEnabled(true);
	}
}