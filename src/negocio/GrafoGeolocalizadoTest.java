package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import negocio.GrafoGeolocalizado.Coordenadas;


public class GrafoGeolocalizadoTest {

	@Test
	public void constructorSimple()
	{
		GrafoGeolocalizado grafo=new GrafoGeolocalizado(4);
		assertTrue(grafo.getCoordenadas(1).getLongitud()==0.0);
		assertTrue(grafo.getCoordenadas(3).getLatitud()==0.0);
		assertFalse(grafo.getCoordenadas(2).getLatitud()!=0.0);
		assertFalse(grafo.getCoordenadas(0).getLatitud()!=0.0);
	}

	@Test (expected = IllegalArgumentException.class)
	public void constructorLimites()
	{
		GrafoGeolocalizado grafo = new GrafoGeolocalizado(-4);
		grafo.getCoordenadas(1);
	}
	
	@Test
	public void constructorConLectura()
	{
		GrafoGeolocalizado grafo=crearGrafoConLectura("instancia1");//la instancia de ejemplo que nos dieron
		assertTrue(-34.52133782929332==grafo.getCoordenadas(0).getLatitud());
		assertTrue(-58.70068073272705==grafo.getCoordenadas(0).getLongitud());
		assertTrue(-34.520772089706036==grafo.getCoordenadas(1).getLatitud());
		assertTrue(-58.702311515808105==grafo.getCoordenadas(1).getLongitud());
		assertTrue(-34.52126711205503==grafo.getCoordenadas(2).getLatitud());
		assertTrue(-58.70325565338135==grafo.getCoordenadas(2).getLongitud());
		assertFalse(-58.70325565338132==grafo.getCoordenadas(2).getLongitud());
		assertFalse(-34.52126711205502==grafo.getCoordenadas(2).getLatitud());
		assertNotEquals(-58.70231151580810,grafo.getCoordenadas(1).getLongitud());
		assertFalse(-34.521772089706032==grafo.getCoordenadas(1).getLatitud());
	}
	
	@Test
	public void equalsCoordenadas()
	{
		Coordenadas c= new Coordenadas(10.2, 10.2);
		Coordenadas c2= new Coordenadas(10.2, 10.2);
		Coordenadas c3= new Coordenadas(11.2, 10.2);
		Coordenadas c4= new Coordenadas(10.2, 11.2);
		Object o=new Object();
		Object cor=new Coordenadas(10.2, 10.2);
		Object cor1=new Coordenadas(10.2, 10.2);
		Coordenadas n=null;
		
		assertEquals(c, c2);
		assertEquals(c, cor);
		assertEquals(cor, cor1);
		assertNotEquals(c, c3);
		assertNotEquals(c, c4);
		assertNotEquals(c, o);
		assertNotEquals(c2, c3);
		assertNotEquals(c2, c4);
		assertNotEquals(c4, c3);
		assertNotEquals(c, n);
	}

	@Test (expected=IllegalArgumentException.class)
	public void checkearExtremosVertices()
	{
		GrafoGeolocalizado grafo=crearGrafoSinLectura();
		grafo.getCoordenadas(-1);
		grafo.getCoordenadas(4);
		grafo.checkearExtremosVertice(-1);
		grafo.checkearExtremosVertice(5);
		grafo.checkearExtremosVertice(4);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void checkearExtremosAristas()
	{
		GrafoGeolocalizado grafo=crearGrafoSinLectura();
		grafo.agregarArista(0, -1);
		grafo.agregarArista(0, 5);
		grafo.checkearExtremosAristas(5, 5);
		grafo.checkearExtremosAristas(5, 4);
		grafo.checkearExtremosAristas(4, 5);
		grafo.checkearExtremosAristas(2, -1);
		grafo.checkearExtremosAristas(-1, 0);
	}
	
	@Test 
	public void setCoordenadas()
	{
		GrafoGeolocalizado grafo=crearGrafoSinLectura();
		assertTrue(1.2==grafo.getCoordenadas(0).getLatitud());
		assertTrue(3.2==grafo.getCoordenadas(0).getLongitud());
		assertTrue(2.1==grafo.getCoordenadas(1).getLatitud());
		assertTrue(3.3==grafo.getCoordenadas(1).getLongitud());
		assertFalse(5.2==grafo.getCoordenadas(2).getLatitud());
		assertFalse(5.2==grafo.getCoordenadas(2).getLongitud());
	}
	
	@Test
	public void agregarContieneArista()
	{
		GrafoGeolocalizado grafo = crearGrafoSinLectura();
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(0, 3);
		
		assertTrue(grafo.contieneArista(0, 1));
		assertTrue(grafo.contieneArista(1, 0));
		assertTrue(grafo.contieneArista(2, 1));
		assertTrue(grafo.contieneArista(1, 2));
		assertTrue(grafo.contieneArista(0, 3));
		
		assertFalse(grafo.contieneArista(0, 2));
		assertFalse(grafo.contieneArista(1, 3));
		assertFalse(grafo.contieneArista(3, 1));
	}
	
	@Test
	public void quitarArista()
	{
		GrafoGeolocalizado grafo = crearGrafoSinLectura();

		grafo.quitarArista(2, 1);
		grafo.quitarArista(0, 1);
		grafo.quitarArista(3, 0);
		grafo.quitarArista(0, 3);

		assertFalse(grafo.contieneArista(0, 1));
		assertFalse(grafo.contieneArista(1, 0));
		assertFalse(grafo.contieneArista(2, 1));
		assertFalse(grafo.contieneArista(1, 2));
		assertFalse(grafo.contieneArista(0, 3));
		
		assertTrue(grafo.getCantidadAristas()==0);
		assertFalse(grafo.getCantidadAristas()!=0);
	}
	
	@Test
	public void contarArista()
	{
		GrafoGeolocalizado grafo=crearGrafoSinLectura();
		grafo.contarAristas();
		assertTrue(grafo.getCantidadAristas()==3);
	}
	
	@Test
	public void getVecinos()
	{
		GrafoGeolocalizado grafo = crearGrafoSinLectura();
		
		assertTrue(grafo.getVecinosDe(0).get(0)==1);
		assertTrue(grafo.getVecinosDe(0).get(1)==3);
		assertTrue(grafo.getGradoVertice(0)==2);
		assertFalse(grafo.getVecinosDe(0).get(0)==0);
		assertFalse(grafo.getVecinosDe(0).contains((Object )2));
		
	}
	
	@Test
	public void getListaCoordenadas()
	{
		GrafoGeolocalizado grafoG=crearGrafoConLectura("instancia1");
		ArrayList<Coordenadas>cor=grafoG.getListaCoordenadas();
		
		for(int i=0; i<grafoG.getCantidadVertices(); i++)
		{
			assertEquals(cor.get(i), grafoG.getCoordenadas(i));
		}
	}
	
	@Test 
	public void getCantidadVertices()
	{
		GrafoGeolocalizado grafo=crearGrafoSinLectura();
		
		assertTrue(grafo.getCantidadVertices()==4);
	}

	//Metodos auxiliares
	public GrafoGeolocalizado crearGrafoSinLectura()
	{
		GrafoGeolocalizado ret=new GrafoGeolocalizado(4);
		ret.setCoordenadas(0, 1.2, 3.2);
		ret.setCoordenadas(1, 2.1, 3.3);
		ret.setCoordenadas(2, 1.2, 3.2);
		ret.setCoordenadas(3, 1.2, 3.2);
		
		ret.agregarArista(0, 1);
		ret.agregarArista(1, 2);
		ret.agregarArista(0, 3);
		return ret;
	}
	
	public GrafoGeolocalizado crearGrafoConLectura(String archivo)
	{
		GrafoGeolocalizado ret=new GrafoGeolocalizado(archivo);
		return ret;
	}

}
