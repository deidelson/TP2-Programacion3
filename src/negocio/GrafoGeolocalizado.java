package negocio;


import java.util.ArrayList;
import java.util.List;

import datos.Conexion;


public class GrafoGeolocalizado 
{	
	private ArrayList<List<Integer>>vecinos;
	private ArrayList<Coordenadas>coordenadas;
	private Conexion conexion;
	private int aristas;

	public GrafoGeolocalizado(String archivoCoordenadas)
	{
		conexion=new Conexion();
		vecinos=new ArrayList<List<Integer>>();
		coordenadas= leerJson(archivoCoordenadas);
		
		for(int i=0; i<coordenadas.size(); i++)
		{
			vecinos.add(new ArrayList<Integer>());
		}
		agregarTodasLasAristas();
	}

	public GrafoGeolocalizado(int cantidadVertices)
	{
		if(cantidadVertices<0)
			throw new IllegalArgumentException("No se admiten cantidades negativas");
		vecinos=new ArrayList<List<Integer>>();
		coordenadas=new ArrayList<Coordenadas>();
		
		for(int i=0; i<cantidadVertices; i++)
		{
			vecinos.add(new ArrayList<Integer>());
			coordenadas.add(new Coordenadas(0.0,0.0));
		}
	}
	
	//inner para las coordenadas de cada vertice
	public static class Coordenadas
	{
		private double latitud; 
		private double longitud;
		public Coordenadas(double x, double y)
		{
			this.latitud=x;
			this.longitud=y;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || getClass() != obj.getClass())
				return false;
			
			Coordenadas otra = (Coordenadas) obj;
			return longitud == otra.longitud && latitud == otra.latitud;
		}
		
		public double getLongitud()
		{
			return this.longitud;
		}
		
		public double getLatitud()
		{
			return this.latitud;
		}
		
		void setLongitud(double d)
		{
			this.longitud=d;
		}
		
		void setLatitud(double d)
		{
			this.latitud=d;
		}
	}
	
	public void agregarArista(int i, int j)
	{
		if(i!=j && contieneArista(i, j)==false && contieneArista(j, i)==false)
		{
			checkearExtremosAristas(i, j);
			aristas++;
			this.vecinos.get(i).add(j);
			this.vecinos.get(j).add(i);
		}
	}
	
	public void quitarArista(int i, int j)
	{
		if(i!=j)
		{
			checkearExtremosAristas(i, j);
	
			if (contieneArista(i,j))
				aristas--;
			
			this.vecinos.get(i).remove((Object)j);
			this.vecinos.get(j).remove((Object)i);
		}
	}

	public boolean contieneArista(int i, int j)
	{
		checkearExtremosAristas(i, j);
		return vecinos.get(i).contains(j);
	}
	
	public int getCantidadAristas() 
	{
		return aristas;
	}
	
	public void contarAristas()
	{
		int cont=0;
		for(List<Integer> elemento:vecinos)
			cont+=elemento.size();
		cont=cont/2;
		this.aristas=cont;
	}

	public void agregarTodasLasAristas()
	{
		for(int i=0; i<getCantidadVertices(); i++)
		{
			for(int j=0; j<getCantidadVertices(); j++)if(i!=j)
			{
				agregarArista(i, j);
			}
		}
	}
	
	public int getCantidadVertices()
	{
		return this.coordenadas.size();
	}

	public int getGradoVertice(int verticeN)
	{
		return vecinos.get(verticeN).size();
	}
	
	public void setCoordenadas(int vertice, double lat, double lon)
	{
		checkearExtremosVertice(vertice);
		coordenadas.get(vertice).setLatitud(lat);
		coordenadas.get(vertice).setLongitud(lon);
	}
	
	public Coordenadas getCoordenadas(int vertice)
	{
		checkearExtremosVertice(vertice);
		return coordenadas.get(vertice);
	}

	public ArrayList<Coordenadas> getListaCoordenadas()
	{
		return this.coordenadas;
	}

	public List<Integer> getVecinosDe(int vertice)
	{
		checkearExtremosVertice(vertice);
		return this.vecinos.get(vertice);
	}
	
	void checkearExtremosVertice(int vertice)
	{
		if(vertice>getCantidadVertices()-1 || vertice<0)
			throw new IllegalArgumentException("El vertice no existe");
	}
	
	void checkearExtremosAristas(int i, int j)
	{
		if(i>getCantidadVertices()-1 || j>getCantidadVertices()-1 || i<0 || j<0)
			throw new IllegalArgumentException("Incorrecto");
	}
	
	public ArrayList<Coordenadas> leerJson (String nombreDelArchivo)
	{
		return conexion.leer(nombreDelArchivo);
	}

}


