package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import negocio.GrafoGeolocalizado.Coordenadas;

public class GrafoPesadoTest 
{
	@Test
	public void constructorConArchivoYPesos()
	{
		GrafoPesado grafo=new GrafoPesado("instancia1");
		assertTrue(grafo.calcularPeso(0, 1)==grafo.calcularPeso(1, 0));
		assertTrue(grafo.calcularPeso(0, 2)==grafo.calcularPeso(2, 0));
		assertTrue(grafo.calcularPeso(0, 3)==grafo.calcularPeso(3, 0));
		assertTrue(grafo.calcularPeso(0, 4)==grafo.calcularPeso(4, 0));
		assertTrue(grafo.calcularPeso(0, 5)==grafo.calcularPeso(5, 0));
		assertTrue(grafo.calcularPeso(0, 6)==grafo.calcularPeso(6, 0));
		assertTrue(grafo.calcularPeso(2, 3)==grafo.calcularPeso(3, 2));
		assertTrue(grafo.calcularPeso(5, 6)==grafo.calcularPeso(6, 5));
		assertFalse(grafo.calcularPeso(0, 0)!=0.0);
		assertFalse(grafo.calcularPeso(2, 2)!=0.0);
		assertFalse(grafo.calcularPeso(4, 4)!=0.0);
		assertFalse(grafo.calcularPeso(5, 5)!=0.0);
		assertFalse(grafo.calcularPeso(1, 1)!=0.0);
	}
	
	@Test  (expected = Exception.class)
	public void constructorArchivoException()
	{
		GrafoPesado grafo=new GrafoPesado("sarasa");
		grafo.getCantidadAristas();
	}
	
	@Test
	public void constructorConCantidad()
	{
		GrafoPesado grafo = crearGrafoSinLectura();
		
		assertTrue(grafo.getCantidadVertices()==4);//testeado cantidad de vertices
		assertFalse(grafo.getCantidadVertices()==3);
	}
	
	@Test
	public void agregarArista()
	{
		GrafoPesado grafo= crearGrafoSinLectura();
		
		assertTrue(grafo.contieneArista(1, 0));//Tambien testeamos contieneArista
		assertTrue(grafo.contieneArista(0, 1));
		assertTrue(grafo.contieneArista(1, 2));
		assertTrue(grafo.contieneArista(3, 2));
		assertFalse(grafo.contieneArista(1, 3));
		assertFalse(grafo.contieneArista(3, 1));
		assertFalse(grafo.contieneArista(1, 3));
		assertFalse(grafo.contieneArista(0, 3));
		assertFalse(grafo.contieneArista(2, 0));
		
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 3);
		
		assertTrue(grafo.getCoordenadas(2).getLatitud()== grafo.getCoordenadas(3).getLatitud());
		assertTrue(grafo.getPeso(0, 2) == grafo.getPeso(0, 1));
		assertTrue(grafo.getPeso(0, 2) == grafo.getPeso(0, 3));
		assertTrue(grafo.getPeso(1, 2) == grafo.getPeso(3, 1));
	}
	
	@Test
	public void quitarArista()
	{
		GrafoPesado grafo= crearGrafoSinLectura();
		
		assertTrue(grafo.getCantidadAristas()==3);//Tambien testeamos getAristas()
	
		grafo.quitarArista(0, 1);
		grafo.quitarArista(2, 1);
		grafo.quitarArista(3, 2);
		grafo.quitarArista(3, 1);
		
		for(int i=0; i<grafo.getCantidadVertices();i++)
		{
			for(int j=0; j<grafo.getCantidadVertices(); j++)
			{
				assertFalse(grafo.contieneArista(i, j));
			}
		}
		assertTrue(grafo.getCantidadAristas()==0);
	}
	
	@Test
	public void agregarTodasLasAristas()
	{
		GrafoPesado grafo= crearGrafoSinLectura();
		grafo.agregarTodasLasAristas();
		
		for(int i=0; i<grafo.getCantidadVertices(); i++)
		for(int j=0; j<grafo.getCantidadVertices();j++)if(i!=j)
		{
			assertTrue(grafo.contieneArista(i, j));
		}
	}
	
	@Test 
	public void getVecinosDe()
	{
		GrafoPesado grafo=crearGrafoSinLectura();
		
		List<Integer> vecinosDelUno= grafo.getVecinosDe(1);
		
		assertTrue(vecinosDelUno.size()==2);
		assertTrue(vecinosDelUno.get(0)==(Object)0);
		assertTrue(vecinosDelUno.get(1)==(Object)2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void getPesoException()
	{
		GrafoPesado grafo=crearGrafoSinLectura();
		grafo.getPeso(0, 4);
		grafo.getPeso(-1, 3);
		grafo.getPeso(-1, 4);
	}
	
	@Test
	public void verQueCarganIgual()
	{
		GrafoGeolocalizado grafoG=new GrafoGeolocalizado("instancia1");
		GrafoPesado grafoP=new GrafoPesado("instancia1");
		 
		assertEquals(grafoG.getCoordenadas(0), grafoP.getCoordenadas(0));
		assertEquals(grafoG.getCoordenadas(1), grafoP.getCoordenadas(1));
		assertEquals(grafoG.getCoordenadas(2), grafoP.getCoordenadas(2));
		assertEquals(grafoG.getCoordenadas(3), grafoP.getCoordenadas(3));
		assertEquals(grafoG.getCoordenadas(4), grafoP.getCoordenadas(4));
	}
	
	@Test
	public void copiarCoordenadas()
	{
		GrafoPesado grafoP=new GrafoPesado("instancia1");
		GrafoPesado grafo=new GrafoPesado(grafoP.getCantidadVertices());
		grafo.copiarCoordenadas(grafoP);
		
		for(int i=0; i<grafoP.getCantidadVertices();i++)
		{
			assertEquals(grafoP.getCoordenadas(i), grafo.getCoordenadas(i));
		}
	}
	
	@Test
	public void getListaCoordenadas()
	{
		GrafoPesado grafoG=new GrafoPesado("instancia1");
		ArrayList<Coordenadas>cor=grafoG.getListaCoordenadas();
		
		for(int i=0; i<grafoG.getCantidadVertices(); i++)
		{
			assertEquals(cor.get(i), grafoG.getCoordenadas(i));
		}
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void copiarCoordenadasException()
	{
		GrafoPesado grafoP=new GrafoPesado("instancia1");
		GrafoPesado grafo=new GrafoPesado(grafoP.getCantidadVertices()-1);
		grafo.copiarCoordenadas(grafoP);
	}
	
	@Test
	public void pesoPromedio()
	{
		GrafoPesado gp=new GrafoPesado(3);
		gp.agregarArista(0, 1, 2.0);
		gp.agregarArista(1, 2, 2.0);
		
		assertTrue(gp.pesoPromedio()==2.0);
		assertFalse(gp.pesoPromedio()==1.0);
	}
	
	@Test
	public void pesoTotal()
	{
		GrafoPesado grafo= new GrafoPesado(4);
		grafo.setCoordenadas(0, 1, 1);
		grafo.setCoordenadas(1, 1, 0);
		grafo.setCoordenadas(2, 0, 1);
		grafo.setCoordenadas(3, 0, 1);
		grafo.agregarArista(0, 1, 1.0);
		grafo.agregarArista(2, 1, 1.0);
		grafo.agregarArista(3, 2, 1.0);
		assertTrue(3.0==grafo.getPesoTotal());
		assertFalse(6.0==grafo.getPesoTotal());
	}
	
	
	//creando instancias
	public GrafoPesado crearGrafoSinLectura()
	{
		GrafoPesado grafo=new GrafoPesado(4);
		grafo.setCoordenadas(0, 1, 1);
		grafo.setCoordenadas(1, 1, 0);
		grafo.setCoordenadas(2, 0, 1);
		grafo.setCoordenadas(3, 0, 1);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(2, 1);
		grafo.agregarArista(3, 2);
		return grafo;
	}

}
