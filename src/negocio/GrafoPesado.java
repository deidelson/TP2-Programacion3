package negocio;

import java.util.ArrayList;
import java.util.List;

import negocio.GrafoGeolocalizado.Coordenadas;

public class GrafoPesado 
{
	private GrafoGeolocalizado grafo;
	private double [][]pesos;
	
	public GrafoPesado(int cantidadVertices)
	{
		grafo=new GrafoGeolocalizado(cantidadVertices);
		pesos=new double[cantidadVertices][cantidadVertices];
	}
	
	public GrafoPesado()
	{
		
	}
	
	public GrafoPesado(String nombreDelArchivo)
	{
		try
		{
			grafo=new GrafoGeolocalizado(nombreDelArchivo);
			pesos = new double [grafo.getCantidadVertices()][grafo.getCantidadVertices()];
			pesarTodo();
		}
		catch(Exception e)
		{
			e.getMessage();
		}
	}
	
	//Calcula el peso teniendo en cuenta las coordenadas
	public void agregarArista(int verticeI, int verticeJ)
	{
		grafo.agregarArista(verticeI, verticeJ);
		pesos[verticeI][verticeJ]=calcularPeso(verticeI, verticeJ);
		pesos[verticeJ][verticeI]=calcularPeso(verticeJ, verticeI);
	}
	
	//El el usuario decide el peso
	public void agregarArista(int verticeI, int verticeJ, double peso)
	{
		grafo.agregarArista(verticeI, verticeJ);
		pesos[verticeI][verticeJ]=peso;
		pesos[verticeJ][verticeI]=peso;
	}
	
	public void quitarArista(int verticeI, int verticeJ)
	{
		grafo.quitarArista(verticeI, verticeJ);
		pesos[verticeI][verticeJ]=0.0;
		pesos[verticeJ][verticeI]=0.0;
	}
	
	public boolean contieneArista(int verticeI, int verticeJ)
	{
		return grafo.contieneArista(verticeI, verticeJ);
	}
	
	public int getCantidadAristas()
	{
		return grafo.getCantidadAristas();
	}
	
	public void agregarTodasLasAristas()
	{
		grafo.agregarTodasLasAristas();
	}
	
	public int getCantidadVertices()
	{
		return grafo.getCantidadVertices();
	}

	public Coordenadas getCoordenadas(int vertice)
	{
		return grafo.getCoordenadas(vertice);
	}
	
	public ArrayList<Coordenadas>getListaCoordenadas()
	{
		return this.grafo.getListaCoordenadas();
	}
	
	public void setCoordenadas(int vertice, double lat, double lon)
	{
		grafo.setCoordenadas(vertice, lat, lon);
		pesarTodo();
	}
	
	public void copiarCoordenadas(GrafoPesado grafoDesde)
	{
		if(grafoDesde.getCantidadVertices() != this.getCantidadVertices())
			throw new IllegalArgumentException("Los grafos deben tener la misma cantidad de vertices!");
		
		for(int i=0; i<this.getCantidadVertices(); i++)
		{
			grafo.setCoordenadas(i, grafoDesde.getCoordenadas(i).getLatitud(), grafoDesde.getCoordenadas(i).getLongitud());
		}
	}
	
	public List <Integer> getVecinosDe(int vertice)
	{
		return grafo.getVecinosDe(vertice);
	}

	public double calcularPeso(int verticeI, int verticeJ)
	{
		double ret=0.0;
		if(grafo.contieneArista(verticeI, verticeJ))
		{
			double latI=grafo.getCoordenadas(verticeI).getLatitud();
			double lonI=grafo.getCoordenadas(verticeI).getLongitud();
			double latJ=grafo.getCoordenadas(verticeJ).getLatitud();
			double lonJ=grafo.getCoordenadas(verticeJ).getLongitud();
			
			ret=Math.sqrt( ((latI-(latJ))*(latI-(latJ))) + ((lonI-(lonJ))*(lonI-(lonJ))));
		}
		return ret;
	}

	public void pesarTodo()
	{
		for(int i=0; i<grafo.getCantidadVertices();i++)
			{
			for(int j=0; j<grafo.getCantidadVertices();j++)
				{
					if(grafo.contieneArista(i, j))	
						pesos[i][j]=calcularPeso(i, j);
				}
			}
	}
	
	public double getPeso(int verticeI, int verticeJ)
	{
		if(grafo.contieneArista(verticeI, verticeJ)==false)
			throw new IllegalArgumentException("No existe dicha arista");
		return pesos[verticeI][verticeJ];
	}
	
	public double pesoPromedio()
	{
		double suma=0;
		int cantidad=0;
		for(int i=0; i<pesos.length; i++)
		{
			for(int j=0; j<pesos[i].length; j++)if(grafo.contieneArista(i, j))
			{
				if(getPeso(i, j) != 0.0)
				{
					suma+=getPeso(i,j);
					cantidad++;
				}
			}
		}
		return (double)(suma/(double)cantidad);
	}
	
	public double getPesoTotal()
	{
		double suma=0;
		
		for(int i=0; i<pesos.length; i++)
		{
			for(int j=0; j<pesos[i].length; j++)if(grafo.contieneArista(i, j))
			{
				if(getPeso(i, j) != 0.0)
				{
					suma+=getPeso(i,j);
					
				}
			}
		}
		return (suma/2);
	}
}
