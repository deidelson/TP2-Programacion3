package negocio;

import java.util.ArrayList;
import java.util.List;


public class Solver 
{
	public static class Arista
	{
		int verticeConocido;
		int verticeExtraño;
		double peso;
	
		public Arista(int vc, int ve, double p)
		{
			verticeConocido=vc;
			verticeExtraño=ve;
			peso=p;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || getClass() != obj.getClass())
				return false;
			
			Arista otra = (Arista) obj;
			return verticeConocido == otra.verticeConocido && verticeExtraño == otra.verticeExtraño;
		}	
	}
	
	public static GrafoPesado AGM(GrafoPesado grafo)
	{
		GrafoPesado resultado=new GrafoPesado(grafo.getCantidadVertices());
		resultado.copiarCoordenadas(grafo);//Para que se dibuje en el mismo lugar
		List<Integer>verticesConocidos=new ArrayList<Integer>();
		verticesConocidos.add(0);
		
		for(int i=0; i<grafo.getCantidadVertices()-1; i++)
		{
			Arista menor = menorArista(grafo, verticesConocidos);
			resultado.agregarArista(menor.verticeConocido, menor.verticeExtraño, menor.peso);
			verticesConocidos.add(menor.verticeExtraño);
		}
		return resultado;
	}
	
	public static GrafoPesado AGMempezandoPorVertice(GrafoPesado grafo, int verticeN)
	{
		if(verticeN>=grafo.getCantidadVertices() || verticeN<0)
		{
			throw new IllegalArgumentException("Numero no permitido");
		}
		GrafoPesado resultado=new GrafoPesado(grafo.getCantidadVertices());
		resultado.copiarCoordenadas(grafo);//Para que se dibuje en el mismo lugar
		List<Integer>verticesConocidos=new ArrayList<Integer>();
		verticesConocidos.add(verticeN);
		
		for(int i=0; i<grafo.getCantidadVertices()-1; i++)
		{
			Arista menor = menorArista(grafo, verticesConocidos);
			resultado.agregarArista(menor.verticeConocido, menor.verticeExtraño, menor.peso);
			verticesConocidos.add(menor.verticeExtraño);
		}
		return resultado;
	}
	
	public static GrafoPesado clusteringQuitandoPesadas(GrafoPesado grafoAGM, int cantidad)
	{
		GrafoPesado resultado=new GrafoPesado(grafoAGM.getCantidadVertices()); 
		resultado.copiarCoordenadas(grafoAGM);
		resultado=quitarElMasGrande(grafoAGM);
		
		for(int i=0; i<cantidad-1; i++)
		{
			resultado=quitarElMasGrande(resultado);
		}
		return resultado;
	}

	public static GrafoPesado clusteringQuitandoMayoresA(GrafoPesado grafoAGM, double peso)
	{
		GrafoPesado resultado=new GrafoPesado(grafoAGM.getCantidadVertices()); 
		resultado.copiarCoordenadas(grafoAGM);
		
		for(int i=0; i<grafoAGM.getCantidadVertices(); i++)
		{
			for(int j=0; j<grafoAGM.getCantidadVertices(); j++)if(grafoAGM.contieneArista(i, j))
			{
				if(grafoAGM.getPeso(i, j)<peso)
					resultado.agregarArista(i, j);
			}
		}
		return resultado;
	}
	
	public static GrafoPesado clusteringPromedioPorN(GrafoPesado grafoAGM, int n)
	{
		GrafoPesado resultado=new GrafoPesado(grafoAGM.getCantidadVertices()); 
		resultado.copiarCoordenadas(grafoAGM);
		
		double promedio=grafoAGM.pesoPromedio();
		
		for(int i=0; i<grafoAGM.getCantidadVertices();i++)
		{
			for(int j=0; j<grafoAGM.getCantidadVertices();j++) if(grafoAGM.contieneArista(i, j))
			{
				if(grafoAGM.getPeso(i, j)<promedio*n)
				{
					resultado.agregarArista(i, j);
				}
				else
				{
					grafoAGM.quitarArista(i, j);
				}	
			}
		}
		return resultado;
	}
	
	//auxiliares
	static Arista menorArista(GrafoPesado grafo, List<Integer> verticesConocidos)
	{
		Arista ret=new Arista(0, 0, Double.MAX_VALUE);
		
		for(Integer i: verticesConocidos)
		{
			for(Integer j:grafo.getVecinosDe(i))
			{
				if(verticesConocidos.contains(j)==false)
					if(grafo.getPeso(i, j)<ret.peso)
						ret=new Arista(i, j, grafo.getPeso(i, j));
			}
		}
		return ret;
	}
	
	static GrafoPesado quitarElMasGrande(GrafoPesado grafoAGM)
	{
		GrafoPesado resultado=new GrafoPesado(grafoAGM.getCantidadVertices()); 
		
		resultado.copiarCoordenadas(grafoAGM);
		double mayor=pesoAristaMasGrande(grafoAGM);
		boolean yaQuito=false;
			
			for(int i=0; i<grafoAGM.getCantidadVertices(); i++)
			{
				for(int j=0; j<grafoAGM.getCantidadVertices(); j++)if(grafoAGM.contieneArista(i, j))
				{
					if(grafoAGM.getPeso(i, j)<mayor || yaQuito==true)
						resultado.agregarArista(i, j);
					else if(grafoAGM.getPeso(i, j)==mayor)
					{
						yaQuito=true;
						grafoAGM.quitarArista(i, j);
					}	
				}
			}
		return resultado;
	}
	
	static double pesoAristaMasGrande(GrafoPesado grafoAGM)
	{
		double mayor=0.0;
		for(int i=0; i<grafoAGM.getCantidadVertices(); i++)
		{
			for(int j=0; j<grafoAGM.getCantidadVertices(); j++)
			{
				if(grafoAGM.contieneArista(i, j))
				{
					if(grafoAGM.getPeso(i, j)>mayor)
						mayor=grafoAGM.getPeso(i, j);
				}
			}
		}
		return mayor;
	}
}
