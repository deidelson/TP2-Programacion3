package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class SolverTest
{
	
	@Test 
	public void aristaEqualsTest()
	{
		Solver.Arista arista= new Solver.Arista(0, 1, 2);
		Solver.Arista arista1= new Solver.Arista(0, 2, 2);
		Solver.Arista arista2= new Solver.Arista(1, 1, 2);
		GrafoGeolocalizado s= new GrafoGeolocalizado(5);
		
		assertFalse(arista.equals(arista1));
		assertFalse(arista.equals(arista2));
		assertFalse(arista1.equals(arista2));
		assertFalse(arista.equals(arista1));
		assertFalse(arista.equals(s));
		assertEquals(arista, new Solver.Arista(0, 1, 2));
	}
	
	@Test
	public void unExtrañoTest()
	{
		GrafoPesado grafo = instancia();

		ArrayList<Integer> conocidos = new ArrayList<Integer>();
		conocidos.add(0);

		Solver.Arista arista = Solver.menorArista(grafo, conocidos);
		Solver.Arista aristaEsperada = Solver.menorArista(grafo, conocidos);
		assertEquals(aristaEsperada, arista);
		
	}
	
	@Test
	public void tresConocidosTest()
	{
		GrafoPesado grafo = instancia();

		ArrayList<Integer> conocidos = new ArrayList<Integer>();
		conocidos.add(0);
		conocidos.add(1);
		conocidos.add(2);
		
		Solver.Arista arista = Solver.menorArista(grafo, conocidos);
		Solver.Arista aristaEsperada=new Solver.Arista(1, 4, 4.0);
		assertEquals(aristaEsperada, arista);
	}
	
	@Test
	public void agmTest()
	{
		GrafoPesado grafo = instancia();
		GrafoPesado agm = Solver.AGM(grafo);
		
		assertTrue(agm.contieneArista(1, 2));
		assertTrue(agm.getPeso(1, 2)==1.0);
		assertTrue(agm.contieneArista(1, 4));
		assertTrue(agm.getPeso(1, 4)==4.0);
		assertTrue(agm.contieneArista(4 ,3));
		assertTrue(agm.getPeso(4, 3)==-10.0);
		assertTrue(agm.contieneArista(0, 1));
		assertTrue(agm.getPeso(1, 0)==5.0);
		assertEquals(4, agm.getCantidadAristas());
		
		for(int i=0; i<grafo.getCantidadVertices(); i++)
		{
			assertEquals(grafo.getCoordenadas(i), agm.getCoordenadas(i));
		}
	}
	
	@Test
	public void agmEmpezandoDesde()
	{
		GrafoPesado grafo=instancia();
		GrafoPesado agm=Solver.AGM(grafo);
		GrafoPesado agmDesde=Solver.AGMempezandoPorVertice(grafo, 3);
		
		assertTrue(agm.getPesoTotal()==agmDesde.getPesoTotal());
	}

	@Test 
	public void clusteringQuitandoPesadas()
	{
		GrafoPesado grafo= instanciaConCoordenadas();
		GrafoPesado sol=Solver.clusteringQuitandoPesadas(grafo, 2);
	
		assertTrue(sol.contieneArista(1, 2));
		assertTrue(sol.contieneArista(2, 3));
		assertTrue(sol.contieneArista(0, 1));
		assertFalse(sol.contieneArista(0, 3));
		assertFalse(sol.contieneArista(1, 3));
	}
	
	@Test 
	public void clusteringQuitandoPesadas2()//Probamos 2 distancias iguales a ver que pasa
	{
		GrafoPesado grafo= instanciaConCoordenadas();
		grafo.setCoordenadas(2, 10, 10);
		// de esta forma las 2 distancias mas grandes serian 0-2 y 0-3
		//Y verificamos que no borra 3 aristas en caso de que haya
		//2 mayores con un mismo peso
		GrafoPesado sol=Solver.clusteringQuitandoPesadas(grafo, 2);
	
		assertTrue(sol.contieneArista(1, 2));
		assertTrue(sol.contieneArista(2, 3));
		assertTrue(sol.contieneArista(0, 1));
		assertTrue(sol.contieneArista(1, 3));
		assertFalse(sol.contieneArista(0, 2));
		assertFalse(sol.contieneArista(0, 3));
	}
	
	@Test//toDo
	public void clusteringQuitandoMayoresAPromedioPorN()
	{
		GrafoPesado grafo= instanciaConCoordenadas();
		//de esta forma deberian quedar 2 clusters (0-1-2) (3)
		GrafoPesado agm=Solver.AGM(grafo);
		GrafoPesado sol=Solver.clusteringPromedioPorN(agm, 2);
		
		assertTrue(sol.contieneArista(0, 1));
		assertTrue(sol.contieneArista(1, 2));
		assertFalse(sol.contieneArista(1, 3));
		assertFalse(sol.contieneArista(2, 3));
		assertFalse(sol.contieneArista(0, 3));
	}
	
	@Test
	public void clusteringQuitandoMayoresa()
	{
		GrafoPesado grafo= instanciaConCoordenadas();
		GrafoPesado sol = Solver.clusteringQuitandoMayoresA(grafo, 5);
		
		assertTrue(sol.contieneArista(0, 1));
		assertFalse(sol.contieneArista(2, 3));
		assertTrue(sol.contieneArista(0, 2));
		assertFalse(sol.contieneArista(0, 3));
		assertTrue(sol.contieneArista(1, 2));
		assertFalse(sol.contieneArista(3, 1));
	}
	
	@Test 
	public void quitarElMasGrande()
	{
		GrafoPesado grafo=instanciaConCoordenadas();
		GrafoPesado sol= Solver.quitarElMasGrande(grafo);
		
		assertTrue(sol.contieneArista(0, 1));
		assertTrue(sol.contieneArista(0, 2));
		assertTrue(sol.contieneArista(1, 2));
		assertTrue(sol.contieneArista(1, 3));
		assertTrue(sol.contieneArista(2, 3));
		assertFalse(sol.contieneArista(0, 3));
	}
	
	@Test
	public void pesoAristaMasGrande()
	{
		GrafoPesado grafo=instanciaConCoordenadas();
		double mayorPeso=Solver.pesoAristaMasGrande(grafo);
		assertTrue(mayorPeso==grafo.getPeso(0, 3));
	}

	//Instancias
	private GrafoPesado instancia()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(0, 1, 5.0);
		grafo.agregarArista(0, 2, 6.0);
		grafo.agregarArista(0, 3, 10.0);
		grafo.agregarArista(1, 2, 1.0);
		grafo.agregarArista(2, 3, 5.0);
		grafo.agregarArista(1, 4, 4.0);
		grafo.agregarArista(2, 4, 10.0);
		grafo.agregarArista(3, 4, -10.0);
		
		return grafo;
	}
	
	private GrafoPesado instanciaConCoordenadas()
	{
		GrafoPesado grafo=new GrafoPesado(4);
		grafo.setCoordenadas(0, 0.00, 0.00);
		grafo.setCoordenadas(1, 1.00, 1.00);
		grafo.setCoordenadas(2, 3.00, 3.00);
		grafo.setCoordenadas(3, 10.00, 10.00);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		return grafo;
	}

}
