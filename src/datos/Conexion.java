package datos;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import negocio.GrafoGeolocalizado.Coordenadas;
import negocio.GrafoPesado;

public class Conexion 
{
	public Conexion()
	{
		
	}

	public ArrayList<Coordenadas> leer(String nombreDelArchivo)
	{
		ArrayList<Coordenadas> ret=null;
		Gson gson=new Gson();
		nombreDelArchivo+=".json";
		 try
		 {
			 Type tipo = new TypeToken<List<Coordenadas>>(){}.getType();
			 BufferedReader br = new BufferedReader(new FileReader("archivos\\"+nombreDelArchivo));
			 ret=gson.fromJson(br, tipo);
		 }
		 catch (Exception e){}
		 return ret;
	}
	
	public void generarJsonCoordenadas(ArrayList<Coordenadas> lista, String nombreDelArchivo)
	{
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		String json=gson.toJson(lista);
		
		try
		{
			FileWriter writer=new FileWriter("archivos\\"+nombreDelArchivo+".json");
			writer.write(json);
			writer.close();
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void generarJsonGrafoPesado(GrafoPesado grafo, String nombreDelArchivo)
	{
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		String json=gson.toJson(grafo);
		
		try
		{
			FileWriter writer=new FileWriter("archivos\\"+nombreDelArchivo+".json");
			writer.write(json);
			writer.close();
		}
		catch(Exception e){}
	}
	
	public GrafoPesado leerGrafo(String nombreDelArchivo)
	{
		GrafoPesado ret=null;
		Gson gson=new Gson();
		nombreDelArchivo+=".json";
		 try
		 {
			 Type tipo = new TypeToken<GrafoPesado>(){}.getType();
			 BufferedReader br = new BufferedReader(new FileReader("archivos\\"+nombreDelArchivo));
			 ret=gson.fromJson(br, tipo);
		 }
		 catch (Exception e) {}
		 return ret;
	}

}
